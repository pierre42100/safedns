#!/bin/bash
HARD_LIST=./hard-coded-list.txt
BLOCKED_ZONE_NAME=$(pwd)/blockeddomains.db
TARGET_FILE=$(pwd)/live-zones.conf

ONLINE_LIST=https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts

echo Target file: $TARGET_FILE
echo Target zone name for blocked domains: $BLOCKED_ZONE_NAME

add_entry () {
	echo "zone \""$i".\" {type master; file \""$BLOCKED_ZONE_NAME"\";};" >> $TARGET_FILE
}

rm $TARGET_FILE

echo "Add internal zones"
echo "# Internal zones" >> $TARGET_FILE
echo "zone \"ns.safedns.communiquons.org\" {type master; file \"/etc/bind/safedns/db.ns.safedns.communiquons.org\";};" >> $TARGET_FILE


echo "" >> $TARGET_FILE

echo "Add built-in entries"
echo "# Built-in entries" >> $TARGET_FILE
for i in $(cat $HARD_LIST); do
	echo "Add built-in entry: $i"
	add_entry $i;
done;

echo "" >> $TARGET_FILE

echo "Add entries from online source"
echo "# Online sources" >> $TARGET_FILE
for i in $(curl $ONLINE_LIST | grep ^"0.0.0.0 " | cut -d" " -f 2 | grep -v "_"); do
	add_entry $i;
done;
