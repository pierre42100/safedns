;
; BIND data file for local loopback interface
;
$TTL	604800
@	IN	SOA	ns1.ns.safedns.communiquons.org. admin.ns.safedns.communiquons.org. (
			      2		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
	IN	NS	ns1.ns.safedns.communiquons.org.;

ns1.ns.safedns.communiquons.org.	IN	CNAME safedns.communiquons.org.;
